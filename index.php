<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
<div class = 'container'> 
<?php
include 'process.php';

$result = $mysqli->query("SELECT * FROM crud_table");
if(!$result){
    echo 'could not select from table ' .$mysqli->error;
}
echo "<table class = 'table'>
<tr>
<th>ID</th>
<th>Name</th>
<th>Location</th>
<th>Action</th>
</tr>";
while($row = $result->fetch_object()){
    echo "<tr>
    <td>$row->id</td>
    <td>$row->crud_name</td>
    <td>$row->crud_location</td>
    <td>
    <a class = 'btn btn-primary' href = 'index.php?edit=$row->id'>Edit</a> &nbsp
    <a class = 'btn btn-danger' href = 'process.php?delete=$row->id'>Delete</a>
    
    </td>
    </tr>";
}
echo "</table>"
?>
<div class = 'row justify-content-center'>
<form method = "POST" action = "process.php">
<input type = 'hidden' name = 'id' value = "<?php echo $idd; ?>">
<div class = 'form-group'>
<label><b>Name</b></label>
<?php

?>
<input type = 'text' name = u_name class = 'form-control' value = '<?php echo $name; ?>' placeholder = 'Enter your name'>
</div>

<div class = 'form-group'>
<label><b>Location</b></label>
<input type = 'text' name = u_location class = 'form-control' value = '<?php echo $location; ?>' placeholder = 'Enter your location'>
</div>

<div class = 'form-group'>
<?php 
 if ($update==true){
    echo "<button type = 'submit' class = 'btn btn-info' name = 'update'>Update</button>";
}else{
    echo "<button type = 'submit' class = 'btn btn-primary' name = 'save'>Save</button>";
}

?>
</div>

</form>
</div>
</div>
</body>
</html>